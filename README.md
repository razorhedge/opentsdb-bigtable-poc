# opentsdb-bigtable-poc

Proyecto de generacion de implementacion "proof of concept" de la herramienta Open TSDB en Bigtable a traves de Kubernetes.

Para esto, se utilizo la libreria [opentsdb-bigtable](https://github.com/GoogleCloudPlatform/opentsdb-bigtable) desarrollada por Google.

## Concepto

Para el caso de POC, se recolectaron metricas de utilizacion de recursos desde la misma plataforma GCP, de la siguiente forma: 

<img src= "https://cloud.google.com/solutions/images/opentsdb-cloud-platform-1.svg"> </img>

## Setup

En primera instancia, es necesario que existan o sean creadas las siguientes instancias:

- Instancia de Bigtable [Lectura/Escritura/Ambas]
- Cluster de GKE [Autodeployable]

Para realizar el deploy, sera clonado el repositorio a cloud shell, utilizando: 

`git clone https://github.com/GoogleCloudPlatform/opentsdb-bigtable.git`

Entrar al directorio de codigo de muestra: 

`cd opentsdb-bigtable`

Considerando la tabla existente de BigTable, procedemos a crear el cluster de Kubernetes, con los alcances de Bigtable Admin y Bigtable Data Viewer 
(Adicionalmente se pueden pasar scopes de Servicios, Region y Mantenimiento)

```
gcloud container clusters create opentsdb-cluster --scopes \
"https://www.googleapis.com/auth/bigtable.admin",\
"https://www.googleapis.com/auth/bigtable.data"
```

Para el resto del deployment, se utiliza la imagen de docker ubicada en gcr.io/cloud-solutions-images/opentsdb-bigtable:v1

El Dockerfile y ENTRYPOINT se encuentran en la carpeta build del deployment.

## Configmap

El configmap de TSDB se encuentra en el archivo opentsdb-configmap.yaml

``` 
        apiVersion: v1
        kind: ConfigMap
        metadata:
          name: opentsdb-config
        data:
          opentsdb.conf: |
            google.bigtable.project.id = project-id
            google.bigtable.instance.id = opentsdb
            google.bigtable.zone.id = us-central1-f 
            hbase.client.connection.impl = com.google.cloud.bigtable.hbase1_2.BigtableConnection
            google.bigtable.auth.service.account.enable = true
        
            tsd.network.port = 4242
            tsd.core.auto_create_metrics = true
            tsd.core.meta.enable_realtime_ts = true
            tsd.core.meta.enable_realtime_uid = true
            tsd.core.meta.enable_tsuid_tracking = true
            tsd.http.request.enable_chunked = true
            tsd.http.request.max_chunk = 131072
            tsd.storage.fix_duplicates = true
            tsd.storage.enable_compaction = false
            tsd.storage.max_tags = 12
            tsd.http.staticroot = /opentsdb/build/staticroot
            tsd.http.cachedir = /tmp/opentsdb
```
Para crearlo desde Kubernetes solo es necesario llamar al archivo 

`kubectl create -f configmaps/opentsdb-config.yaml`

## Jobs para crear tablas en CBT

Lanzamos un job en GKE para crear las tablas necesarias (existe uno tambien para limpiarlas [opentsdb-clean.yaml]), esto puede tomar varios minutos.

`kubectl create -f jobs/opentsdb-init.yaml`

Para revisar el estado de los jobs, usar:

`kubectl describe jobs`

En caso de tener mas de un proyecto o deployment, utilizar el tag `--namespace` o `-n` con el nombre de la instancia. 

Luego revisamos los pods con los logs de creacion de las tablas: 

```
pods=$(kubectl get pods  --show-all --selector=job-name=opentsdb-init \
--output=jsonpath={.items..metadata.name})

kubectl logs $pods
```

## Deployment

Para probar la lectura y escritura, se realizara el siguiente deployment: 

<img src = "https://cloud.google.com/solutions/images/opentsdb-cloud-platform-2.svg"> </img>

Para la poc se utilizaron 2 deployments, uno para escritura y uno para lectura, configurados por defecto en opentsdb-write.yaml y opentsdb-read.yaml

`kubectl create -f deployments/opentsdb-write.yaml`

`kubectl create -f deployments/opentsdb-read.yaml`

## Servicios

Adicionalmente, creamos los servicios de lectura y escritura:

`kubectl create -f services/opentsdb-write.yaml`

`kubectl create -f services/opentsdb-read.yaml`

## Escritura de series de tiempo

Utilizamos el siguiente deployment de heapster para escribir metricas en serie de tiempo: 


`kubectl create -f deployments/heapster.yaml`

### heapster.yaml

```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: heapster-opentsdb
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: heapster-opentsdb
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:heapster
subjects:
- kind: ServiceAccount
  name: heapster-opentsdb
  namespace: default
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: heapster-opentsdb
spec:
  replicas: 1
  template:
    metadata:
      labels:
        task: monitoring
        app: heapster-opentsdb
    spec:
      serviceAccountName: heapster-opentsdb
      containers:
      - name: heapster-opentsdb
        image: k8s.gcr.io/heapster-amd64:v1.5.3
        imagePullPolicy: IfNotPresent
        command:
        - /heapster
        - --source=kubernetes:https://kubernetes.default
        - --sink=opentsdb:http://opentsdb-write:4242?cluster=opentsdb-cluster
        - --metric_resolution=5s 
```        
Podemos modificar los parametros de ventana de recoleccion de metricas en `--metric_resolution` y la fuente de las mismas en `--source`, siendo posible utilizar Stackdriver u otros backends. 

[Heapster](https://github.com/kubernetes-retired/heapster) se encuentra deprecado en soporte, por lo que una alternativa puede ser migrar a [Prometheus](https://github.com/coreos/prometheus-operator). 

Y observamos el resultado escrito en Grafana:

<img src = "https://gitlab.com/razorhedge/opentsdb-bigtable-poc/raw/master/Grafana_Heapster_Output.png"> </img>

## Limpieza y reciclaje

Para borrar el cluster GKE:

`gcloud container clusters delete opentsdb-cluster`

Para limpiar la data de TSDB: 

`kubectl create -f jobs/opentsdb-clean.yaml`

